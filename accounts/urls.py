from django.urls import path, include
from accounts.views import signup
from accounts.views import user_login 
from accounts.views import to_logout




urlpatterns = [
    path("signup/", signup, name="signup"),
    path("user_login/",user_login, name="user_login"),
    path("logout/", to_logout, name="logout"),
]