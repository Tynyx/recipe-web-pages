"""
URL configuration for scrumptious project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import render, redirect


# This is a function that redirects the browser to the recipe list page in short hand form
# It would regularly look like this:
# def redirect_to_recipes(request):
#     return redirect("recipe_list")
redirect_to_recipes = lambda request: redirect("recipe_list")

urlpatterns = [
    path('admin/', admin.site.urls),
    path("recipes/", include("recipes.urls")),
    path("", redirect_to_recipes, name="home_page"),
    path("accounts/", include("accounts.urls")),
]
