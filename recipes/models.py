from django.db import models
from django.core.validators import MaxValueValidator
from django.conf import settings

# Create your models here.


class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    ingredients = models.TextField()
    rating = models.FloatField(null=True, validators=[MaxValueValidator(10.0)])

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title + " by " + self.author.username


class Recipe_Step(models.Model):
    step_number = models.IntegerField()
    instructions = models.TextField()
    recipe = models.ForeignKey(Recipe, related_name="steps", on_delete=models.CASCADE)

    class Meta:
        ordering = ["step_number"]


class gredients(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name="gredients",
        on_delete=models.CASCADE)

    class Meta:
        ordering = ["food_item"]