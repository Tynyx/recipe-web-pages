from django.contrib import admin
from recipes.models import Recipe
from recipes.models import Recipe_Step
from recipes.models import gredients
# Register your models here.


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",

    ]


@admin.register(Recipe_Step)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = [
        "step_number",
        "id",
        "instructions",

    ]


@admin.register(gredients)
class gredientsAdmin(admin.ModelAdmin):
    list_display = [
        "amount",
        "recipe",
        "food_item",
    ]