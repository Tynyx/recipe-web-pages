from django.shortcuts import render, get_object_or_404, redirect 
from recipes.models import Recipe
from recipes.forms import RecipeForm
from recipes.forms import PostForm
from django.contrib.auth.decorators import login_required


# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, 'recipes/detail.html', context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, 'recipes/list.html', context)


@login_required
def create_recipe(request):
    if request.method == "POST":
        #we should use the form to validate the values
        # and save them to the database
        
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            # If all goes well, we can redirect the browser
            # to another page and leave the function
            return redirect("recipe_list")
    else:
        # Create an instance of the Django model form class
        form = RecipeForm()    
    # Put the form in the context
    context = {
        "form": form,
    }
    # render the HTML template with the form
    return render(request, "recipes/create.html", context)


def update_recipe(request, id):
    # Get the object that we want to edit 
    post = get_object_or_404("Post", id=id) 
    if request.method == "POST":
          
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("show_recipe",)
        # POST is when the person has submitted the form
        # We should use the form to validate the values
        # and save them to the database
        # If all goes well, we can reditect th browser
        # to another page
    else: 
        form = PostForm(instance=post)
    context = {
        "post_object": post,
        "form": form,
    }
    return render(request, "recipes/update.html", context)
        # Creat an instance of the Django model from class
        # with the object that the person wants to edit

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, 'recipes/list.html', context)


