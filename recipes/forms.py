from django.forms import ModelForm
from recipes.models import Recipe


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            'title',
            'picture',
            'description',
            'ingredients', 
            'rating'
        ]

 
class PostForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            'title',
            'picture',
            'description',
            'ingredients', 
            'rating'
        ]